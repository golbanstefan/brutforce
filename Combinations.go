package main

import (
	"fmt"
	"strings"
)

func getAllCombinations() []string {
	var combResult []string
	comb(9, 4, func(c []int) {
		d := strings.Trim(strings.Replace(fmt.Sprint(c), " ", "", -1), "[]")
		combResult = append(combResult, d)
	})
	return combResult
}

func comb(n, m int, emit func([]int)) {
	s := make([]int, m)
	last := m - 1
	var rc func(int, int)
	rc = func(i, next int) {
		for j := next; j <= n; j++ {
			s[i] = j
			if i == last {
				emit(s)
			} else {
				rc(i+1, j)
			}
		}
		return
	}
	rc(0, 0)
}
