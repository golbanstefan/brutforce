package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"sync"
)

func DoRequest(c Credentials, ch chan Result, wg *sync.WaitGroup) {
	defer (*wg).Done()
	data := url.Values{
		"username": {c.Username},
		"pin":      {c.pin},
	}
	resp, err := http.PostForm(c.Url, data)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	var result Result
	err = json.Unmarshal(body, &result.Response)
	if err != nil {
		log.Fatalln(err)
	}
	if result.Response.Status != "fail" {
		result.Credentials = c
		ch <- result
		close(ch)
	}

}
