package main

type Credentials struct {
	Url      string
	Username string
	pin      string
}

func (c *Credentials) InitData() {
	c.Url = "http://30cm.org/stefan_login.php"
	c.Username = "secretuser"
}

type Response struct {
	Status string
}
type Result struct {
	Credentials Credentials
	Response    Response
}
